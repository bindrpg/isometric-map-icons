# Isometric Map Icons

[![Creative Commons License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-sa/4.0/)

This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License]("http://creativecommons.org/licenses/by-sa/4.0/)

## Ch-Ch-Changes

If you want to add resources, try to separate items into the correct layer, whenever possible (it helps with git merges immensely).
